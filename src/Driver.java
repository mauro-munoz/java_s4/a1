public class Driver {

//    public void show(){
//        Car carSample = new Car();
//        carSample.make = "Crosswind";
//        System.out.println(carSample.make);
//    }
        private String name;
        private String address;
        private Integer age;

        public Driver(){};
        public Driver(Integer age, String name, String address){
            this.name = name;
            this.address = address;
            this.age = age;
        }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }
}
