public class Car {

    //Attributes and Methods
    //public - the variable in the class is accessible anywhere in the application
    // setters are public methods which will allow us to set the value of the attribute of an instance
    // getters are public methods which will allow us to get the value of the attribute of an instance.
    private String make;
    private String brand;
    private int price;
    private Driver carDriver;

// Class attributes/properties must not be public, they should only be accessed and set via class methods called setters and getters.
    // void- means that the function does not return anything. because in Java, a function/method returns dataTypes must be declared.
    // private - limit the access and ability to set a variable/method to its class.
    public void start(){
        System.out.println("tugs tugs tugs");
    }
    //parameters in java needs its dataType declared
//    public void setMake(String makeParams){
//        //this keyword refers to the object where the constructor or setter is.
//        this.make = makeParams;
//    }

//    public void setBrand(String brandParams){
//        //this keyword refers to the object where the constructor or setter is.
//        this.brand = brandParams;
//    }
//    public void setPrice(Integer priceParams){
//        //this keyword refers to the object where the constructor or setter is.
//        this.price = priceParams;
//    }
//    public String getMake(){
//        return this.make;
//    }
//    public String getBrand(){
//        return this.brand;
//    }
//    public Integer getPrice(){
//        return this.price;
//    }


    public String getMake() {
        return make;
    }

    public String getBrand() {
        return brand;
    }

//    public void setPrice(int price) {
//        this.price = price;
//    }

//constructor is a method which allows us to set the initial values of an instance.
    // empty/default constructor - default constructor allows us to create an instance with default initialized values for our properties.
    public Car(){
        //empty or you can designate default values instead of getting them from parameters.
        //Java already creates one for us. however empty/default constructor made by java allows java to set its own default values. if you create your own you can create your own default values

    }
    //parameterized constructor - allows us to initialize values to our attributes upon creation of the instance.
    public Car(Integer price, String make, String brand, Driver driver){
        this.make = make;
        this.brand = brand;
        this.price = price;
        this.carDriver = driver;
    }
    public int getPrice() {
        return price;
    }

//    public void setBrand(String brand) {
//        this.brand = brand;
//    }


    public void setMake(String make) {
        this.make = make;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public String getCarDriverName(){
        return this.carDriver.getName();
    }


}
