public class Main {
//    public String sample = "I am a sample";

    public static void main(String[] args) {

//        Main newSample = new Main();
//        System.out.println(newSample.sample);

        Car car1 = new Car();

//        car1.brand = "Toyota";
//        car1.make = "Innova";
//        car1.price = 1000;
//
//        System.out.println(car1.brand);
//        System.out.println(car1.make);
//        System.out.println(car1.price);

        Car car2 = new Car();
//        car2.brand = "Toyota";
//        car2.make = "Rav";
//        car2.price = 2000;
////        System.out.println(car2.brand + " " + car2.make + " " + car2.price);
//        System.out.println(car2.brand);
//        System.out.println(car2.make);
//        System.out.println(car2.price);

//        car2.owner = "Mang Jose" we cannot add a new property that is not described in the Class.

        Car car3 = new Car();
//        car3.brand = "Honda";
//        car3.make = "Civic";
//        car3.price = 5000;
//
//
//        System.out.println(car3.brand);
//        System.out.println(car3.make);
//        System.out.println(car3.price);
        car3.start();


//        car1.setMake("Innova");
        System.out.println(car1.getMake());
//        car1.setBrand("Toyota");
        System.out.println(car1.getBrand());
//        car1.setPrice(1000);
        System.out.println(car1.getPrice());

        Car car4 = new Car();
        System.out.println(car4.getBrand());

        Driver driver1 = new Driver(30,"Mauro","Baguio");
        System.out.println(driver1.getAddress());
        System.out.println(driver1.getAge());
        System.out.println(driver1.getName());

        Car car5 = new Car(2000,"Corolla","Toyota",driver1);
        System.out.println(car5.getMake());
        System.out.println(car5.getBrand());
        System.out.println(car5.getPrice());
        System.out.println(car5.getCarDriverName());

        Animal animal1 = new Animal("Dog","Brown");
//        System.out.println(animal1.call());
        animal1.call();
    }


}